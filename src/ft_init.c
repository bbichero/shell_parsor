#include "../include/parsor_private.h"

t_parse			*ft_init_parse_struct(void)
{
	t_parse		*parse;

	parse = (t_parse *)malloc(sizeof(t_parse));
	parse->next = NULL;
	parse->command = NULL;
	parse->line = NULL;
	parse->delimitor = (t_delimitor *)malloc(sizeof(t_delimitor));
	return (parse);
}

t_delimitor		*ft_init_delimitor_struct(void)
{
	t_delimitor	*delimitor;

	delimitor = (t_delimitor *)malloc(sizeof(t_delimitor));
	delimitor->next = NULL;
	delimitor->content = NULL;
	delimitor->first_d = 0;
	delimitor->last_d = 0;
	delimitor->simple_q = 0;
	delimitor->double_q = 0;
//	delimitor->
//	delimitor->
//	delimitor->
	return (delimitor);
}
