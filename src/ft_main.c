#include "../include/parsor_private.h"

int			main(int ac, char **av)
{
	t_parse 	*parse;
	t_parse		*tmp;
	int		ret;
	char		*line;
	char		buff[32];

	(void)av;
	parse = ft_init_parse_struct();
	parse->delimitor = ft_init_delimitor_struct();
	tmp = parse;
	if (ac != 1)
		return (0);
	ft_putstr("$> ");
	ft_bzero(buff, 32);
	if (read(1, buff, 32) > 0)
	{
		line = ft_strtrim(buff);	
		while ((ret = ft_parse_line(line, parse)) == 1)
		{
			ft_putstr("> ");
			if (read(1, buff, 32) == 0)
			{
				parse->next = ft_init_parse_struct();
				parse->next->delimitor = ft_init_delimitor_struct();
				parse = parse->next;
			}
			ft_bzero(buff, 32);
			line = ft_strtrim(buff);	
		//	ft_putstr("line: ");
		//	ft_putendl(line);
		}
		while (tmp)
		{
			// execute commands
			ft_putstr("line : ");
			ft_putendl(line);
			if (tmp->command)
			{
				ft_putstr("commands : ");
				ft_putendl(tmp->command);
			}
			tmp = tmp->next;
		}
		free(line);
	}
	free(parse);
	return (0);
}
