#ifndef PARSOR_PUBLIC_H
# define PARSOR_PUBLIC_H

# include "../libft/libft.h"
# include <stdio.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>

/*
** Enum to handle different state of a command
** - ERROR : command contain error, can't be executed
** - TO_EXECUTE : command can be executed correctly
** - SAVE_OUTPUT : output must be stock
** - SAVE_RESULT : 
** - BUIL_IN : command is a built-in
** - LOCAL_VAR : save or edit local var
** - ENV_VAR : save or edit env var
*/

typedef enum			e_cmd_state
{
	ERROR = 0,
	TO_EXECUTE = 1,
	SAVE_OUTPUT = 2,
	SAVE_RESULT = 3,
	BUILT_IN = 4,
	LOCAL_VAR = 5,
	ENV_VAR = 6
}				t_cmd_state;

/*
** s_parse struct is use to handle parsing of
** line read from shell prompt.
** It stock delimitors, commands to execute,
** variable set, etc .. All instructions that
** must be excuted by this program.
*/

typedef struct			s_parse
{
	struct s_parse		*next;
	struct s_delimitor	*delimitor;
	char			*line;
	char			*command;
	char			*argument;
	t_cmd_state		cmd_state;
}				t_parse;

/*
** s_delimitor struct is use to handle multiple
** delimitors in shell command, by stocking
** each number of char containor in every lines.
*/

typedef struct			s_delimitor
{
	struct s_delimitor	*next;
	char			*content;
	char			first_d;
	char			last_d;
	int			simple_q;
	int			double_q;
	int			bracie_l;
	int			bracie_r;
	int			parent_l;
	int			parent_r;
}				t_delimitor;

typedef struct			s_loc_var
{
	struct s_loc_var	*next;
	char			*name;
	char			*value;
}				t_loc_var;

typedef struct			s_env
{
	struct s_env		*next;
	char			*name;
	char			*value;
}				t_env;

typedef struct			s_path
{
	struct s_path		*next;
	char			*value;
	int			valid;
}				t_path;

typedef struct			s_group
{
	t_env			*env;
	t_loc_var		*loc_var;
	char			*name;
	char			*value;
}				t_group;

/*
** t_group *group => struct containing loc_var struct
** char *name => local var name
** char *value => local var value
*/
void				ft_add_local_var(t_group *group, char *name, char *value);

/*
** t_group *group => struct containing loc_var struct
** char *name => local var name
*/
void				ft_del_loc_var(t_group *group, char *name);

/*
** char *line => line from STDIN
** t_parse *parse => parse struct with all commands values, setting
*/
int				ft_find_delimitors(char *line, t_parse *parse);

/*
** char *line => line from STDIN
** t_parse *parse => parse struct with all commands values, setting
*/

int				ft_need_more_read(char *line, t_parse *parse);

/*
** char *line => line from STDIN
** t_parse *parse => parse struct with all commands values, setting
*/

int				ft_parse_line(char *line, t_parse *parse);

t_parse				*ft_init_parse_struct(void);
t_delimitor			*ft_init_delimitor_struct(void);

#endif
